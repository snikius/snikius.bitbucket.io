import {Task} from "../objects/task";
import {Popup} from "../objects/popup";
import {report} from "./monitoring";

export class GameScene extends Phaser.Scene {
  private bricks: Phaser.Physics.Arcade.StaticGroup;
  private ball: Phaser.Physics.Arcade.Image;
  private paddle: Phaser.Physics.Arcade.Image;

  private tasks: Task[][];

  private popup: Popup;
  private popupWin: Popup;
  private popupToMany: Popup;

  private texts: Phaser.GameObjects.Text[] = [];

  private lives: number = 3;
  private readonly livesCount: number = 3;

  private playEvent:  Phaser.Time.TimerEvent = null;

  private eventsList: Phaser.Events.EventEmitter[] = [];

  constructor() {
    super({
      key: "GameScene"
    });
  }

  preload(): void {

  }

  init(): void {

  }

  getTask(
      scene: Phaser.Scene,
      group: Phaser.Physics.Arcade.StaticGroup,
      positionX: number,
      positionY: number,
      asset: string,
      frame: string = null,
  ): Task {
      let task = new Task(scene, group, positionX, positionY, asset, frame);
      task.obj.setData('task', task);
      return task;
  }

  buildBord(): void {
      this.tasks = [
          [
              this.getTask(this, this.bricks, 0, 0, 'i1'),
              null,
              null,
              null,
              null,
              null,
              null,
              null,
          ],
          [
              this.getTask(this, this.bricks, 1, 0, 'i4'),
              this.getTask(this, this.bricks, 1, 1, 'i3'),
              null,
              null,
              null,
              null,
              null,
              null,
          ],
          [
              this.getTask(this, this.bricks, 2, 0, 'i2'),
              this.getTask(this, this.bricks, 2, 1, 'i5'),
              this.getTask(this, this.bricks, 2, 2, 'i6'),
              null,
              null,
              null,
              null,
              null,
          ],
          [
              this.getTask(this, this.bricks, 3, 0, 'i7'),
              this.getTask(this, this.bricks, 3, 1, 'i8'),
              null,
              null,
              null,
              null,
              null,
              null,
          ],
          [
              this.getTask(this, this.bricks, 4, 0, 'i9'),
              null,
              null,
              null,
              null,
              null,
              null,
              null,
          ],
          [
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
          ],
      ];
  }

  create(): void {
      this.popup = new Popup(this, this.sys.canvas.width, this.sys.canvas.height,
          "Вы проиграли, попробуйте заново.",
          "Упс!",
          "Начать заново", () => {
              this.closePopupRetry();
          });
      this.popupToMany = new Popup(this, this.sys.canvas.width, this.sys.canvas.height,
          "Плохой аджаил! Вы накопили слишком много задач в одном статусе! Попробуйте заново.",
          "Упс!",
          "Начать заново", () => {
              this.closePopupRetry();
          });
      this.popupWin = new Popup(this, this.sys.canvas.width, this.sys.canvas.height,
          "Спасибо за участие! Вашу помощь сложно недооценить. \n\n Мы обязательно донесем информацию о Ваших успехах руководству. \n\n С Новым годом! =)",
          "Победа!",
          "Начать заново", () => {
              this.closePopupRetry();
          });
      this.add.tileSprite(0, 0, 11000, 6700, 'background');

      //  Enable world bounds, but disable the floor
      this.physics.world.setBoundsCollision(true, true, true, false);

      this.bricks = this.physics.add.staticGroup();

      this.buildBord();

      this.ball = this.physics.add.image(400, 640, 'assets', 'ball1').setCollideWorldBounds(true).setBounce(1);
      this.ball.setData('onPaddle', true);

      this.paddle = this.physics.add.image(400, 665, 'assets', 'paddle1').setImmovable();

      //  Our colliders
      this.physics.add.collider(this.ball, this.bricks, this.hitBrick, null, this);
      this.physics.add.collider(this.ball, this.paddle, this.hitPaddle, null, this);

      //  Input events
      this.initInput();

      this.texts.push(
          this.add.text(this.sys.canvas.width - 270, 25, "Жизней: " + this.lives, {
              fontFamily: "Arial",
              fontSize: "20px",
              fill: "#172B4D"
          })
      );

      this.playEvent = this.time.addEvent({ delay: 999999, loop: true });
    }

    initInput = () => {
        this.eventsList.push(
            this.input.on('pointermove',  (pointer) => {
                //  Keep the paddle within the game
                this.paddle.x = Phaser.Math.Clamp(pointer.x, 52, 1044);

                if (this.ball.getData('onPaddle')) {
                    this.ball.x = this.paddle.x;
                }

            }, this)
        );
        this.eventsList.push(
            this.input.on('pointerdown',(pointer) => {
                if (this.ball.getData('onPaddle')) {
                    this.ball.setVelocity(-75, -300);
                    this.ball.setData('onPaddle', false);
                }
            }, this)
        );
    };

    disableInput = () => {
        this.eventsList.forEach((event) => {
            event.removeAllListeners();
        });
        this.eventsList = [];
    };

    enableInput = () => {
        this.initInput();
    };

    recalculateBord = (task: Task): Phaser.Math.Vector2 => {
      let bordKeyX = task.positionX;
      let bordKeyY = task.positionY;

      let bordNextKeyX = task.positionX + 1;

      if (this.tasks[bordKeyX]) {
          this.tasks[bordKeyX][bordKeyY] = null;
      }
      // Ищем свободную позицию в след блоке по вертикали.
      for (let placeY = 0; placeY < 10; placeY++) {
        // След. колонка
        if (!this.tasks[bordNextKeyX][placeY]) {
            // Установим туда блок
            this.tasks[bordNextKeyX][placeY] = task;
            // Возвращаем с новую позицию
            return new Phaser.Math.Vector2(bordNextKeyX, placeY);
        }
      }
      return null;
    };

    // Очистка послед столбца
    clearLast = (task: Task) => {
        this.tasks[4].forEach((t, index) => {
            if (task === t) {
                this.tasks[4][index] = null;
            }
        });
        task.clear();
    };

    shake = () => {
        this.cameras.main.shake(250, 0.001);
    };

    hitBrick = (ball, brick) => {
        let task: Task = brick.getData('task');
        let newPosition: Phaser.Math.Vector2 = null;
        if (task.isActive()) {
            if (task.positionX < 4) {
                newPosition = this.recalculateBord(task);
            } else {
                this.clearLast(task);
            }
        }
        if (newPosition) {
            task.moveToPosition(newPosition.x, newPosition.y);
            // Проиграли, слишком большая колонка
            if (newPosition.y > 5) {
                this.finishWithToMany();
                return;
            }
        }
        if (this.bricks.countActive() === 0) {
            this.resetLevel();
            this.shake();
            this.openWinPopup();
        }
    };

    openPopup = () => {
        this.disableInput();
        this.popup.show();
    };

    openWinPopup = () => {
        report("victory");
        this.disableInput();
        this.popupWin.show();
        this.popupWin.additionalText
            && this.popupWin.additionalText.setText("Времени потрачено: " + this.playEvent.getElapsedSeconds().toString().substr(0, 4) + " сек.");
    };

    closePopup = () => {
        this.enableInput();
    };

    closePopupRetry = () => {
        report("retry");
        this.closePopup();
        this.resetLevel();
        this.playEvent.reset({ delay: 999999, loop: true });
    };

    finishWithFail = () => {
        report("fail");
        this.resetLevel();
        this.shake();
        this.openPopup();
    };

    finishWithToMany = () => {
        this.resetLevel();
        this.disableInput();
        this.popupToMany.show();
    };

    resetTry = (fail: boolean) => {
        this.ball.setVelocity(0);
        this.ball.setPosition(this.paddle.x, 640);
        this.ball.setData('onPaddle', true);
        if (fail) {
            if (this.lives == 0) {
                this.finishWithFail();
            } else {
                this.shake();
                this.setLives(this.lives - 1);
            }
        }
    };

    setLives = (lives: number) => {
        this.lives = lives;
        this.texts[0].setText("Жизней: " + lives);
    };

    resetLevel = () => {
        this.setLives(this.livesCount);
        this.resetTry(false);
        this.bricks.clear(true, true);
        this.bricks = this.physics.add.staticGroup();
        this.buildBord();
        this.physics.add.collider(this.ball, this.bricks, this.hitBrick, null, this);
    };

    hitPaddle = (ball, paddle) => {
        let diff = 0;

        if (ball.x < paddle.x) {
            //  Ball is on the left-hand side of the paddle
            diff = paddle.x - ball.x;
            ball.setVelocityX(-12 * diff);
        }
        else if (ball.x > paddle.x) {
            //  Ball is on the right-hand side of the paddle
            diff = ball.x -paddle.x;
            ball.setVelocityX(12 * diff);
        }
        else {
            //  Ball is perfectly in the middle
            //  Add a little random X to stop it bouncing straight up!
            ball.setVelocityX(2 + Math.random() * 10);
        }
        // Проверим что скорость по верт. не может упасть слишком низко
        if (ball.body.velocity && ball.body.velocity.y > -30 && ball.body.velocity.y < 0) {
            ball.setVelocityY(-55);
        }
    };

    update(): void {
        //console.log('bord', this.tasks);
        if (this.ball.y > 665) {
            this.resetTry(true);
        }
    }

}
