import {report} from "./monitoring";

export class MainMenuScene extends Phaser.Scene {
  private texts: Phaser.GameObjects.Text[] = [];

  private btn1: Phaser.GameObjects.Graphics;

  constructor() {
    super({
      key: "MainMenuScene"
    });
  }

  init(): void {
  }

  preload(): void {
      let loading = this.make.text({
          x: 550,
          y: 320,
          text: "ЗАГРУЗКА: 0%",
          origin: { x: 0.5, y: 0 },
          style: {
              font: '26px Arial',
              fill: "#FFFFFF",
              align: 'center',
              wordWrap: { width: 400 }
          }
      });

      this.load.on('progress', function (value) {
          //console.log('', value, `ЗАГРУЗКА: ${Math.round(value * 100)}%`);
          loading.setText(`ЗАГРУЗКА: ${Math.round(value * 100)}%`);
      });

      this.load.on('complete', function () {
          report("loading_finished");
          loading.destroy();
      });

      this.load.image(
          "background",
          "./assets/gamebg.jpg"
      );

      this.load.image("i1", "./assets/i1.jpg");
      this.load.image("i2", "./assets/i2.jpg");
      this.load.image("i3", "./assets/i3.jpg");
      this.load.image("i4", "./assets/i4.jpg");
      this.load.image("i5", "./assets/i5.jpg");
      this.load.image("i6", "./assets/i6.jpg");
      this.load.image("i7", "./assets/i7.jpg");
      this.load.image("i8", "./assets/i8.jpg");
      this.load.image("i9", "./assets/i9.jpg");

      this.load.image("hat", "./assets/hat_mini.png");

      this.load.image(
          "background2",
          "./assets/gamebg2.jpg"
      );

      this.load.atlas(
          'assets',
          'assets/frames.png',
          'assets/data.json'
      );
  }

  create(): void {
      this.add.tileSprite(0, 0, 11000, 6700, 'background2');

      this.texts.push(this.make.text({
          x: this.sys.canvas.width * 0.5,
          y: this.sys.canvas.height * 0.5 - 275,
          text: "СберТех нуждается в тебе!\n\n" +
          "К концу года мы не успели закрыть ряд очень важных задач." +
          "\nНачни игру и закрой все задачи, очистив последнюю колонку.",
          origin: { x: 0.5, y: 0 },
          style: {
              font: '18px Arial',
              fill: "#172B4D",
              align: 'center',
              wordWrap: { width: 450 }
          }
      }));

      let rectBtn1 = new Phaser.Geom.Rectangle(this.sys.canvas.width * 0.5 - 100,
          this.sys.canvas.height * 0.5 + 140, 160, 35);
      this.btn1 = this.add.graphics({ fillStyle: { color: 0x0052cc } } as any);
      this.btn1.setInteractive(rectBtn1, Phaser.Geom.Rectangle.Contains);
      this.btn1.fillRectShape(rectBtn1);

      this.texts.push(this.make.text({
          x: this.sys.canvas.width * 0.5 - 20,
          y: this.sys.canvas.height * 0.5 + 144,
          text: "Начать игру!",
          origin: { x: 0.5, y: 0 },
          style: {
              font: '20px Arial',
              fill: "#F5F5F5",
              align: 'center',
              wordWrap: { width: 200 }
          }
      }));

      this.btn1.on('pointerdown',  () => {
          this.scene.start("GameScene");
      });
  }

  update(): void {

  }
}
