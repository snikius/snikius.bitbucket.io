import axios from 'axios';
import {MONITORING_URL} from "../../../../monitoring";


export function report(event) {
    const url = MONITORING_URL.replace("$1", event);
    console.log("Отправка события в мониторинг: ", event);
    // URL для мониторинга тут
    axios.get(url)
        .then(function (response) {
            console.log("Результат отправки события: ", response);
        }).catch((e) => {
        console.log("Ошибка отправки события: ", e);
        });
}