class Popup {
    private bg: Phaser.GameObjects.Graphics;
    private info: Phaser.GameObjects.Graphics;
    private btn1: Phaser.GameObjects.Graphics;
    private btn2: Phaser.GameObjects.Graphics;

    private header: string;
    private text: string;
    public additionalText: Phaser.GameObjects.Text;

    private btn1Text: string;
    private btn2Text: string;

    private btn1Call: () =>  any;
    private btn2Call: () =>  any;

    private texts: Phaser.GameObjects.Text[] = [];

    private hat: Phaser.GameObjects.Sprite;

    private w: number;
    private h: number;
    private scene: Phaser.Scene;

    private isShow: boolean;

    constructor(scene: Phaser.Scene, w: number, h: number, text: string, header: string, btn1?: string, btn1Call?: () => any, btn2?: string, btn2Call?: () => any) {
        this.scene = scene;
        this.h = h;
        this.w = w;

        this.header = header;
        this.text = text;
        this.btn1Text = btn1;
        this.btn2Text = btn2;

        this.btn1Call = btn1Call;
        this.btn2Call = btn2Call;

        this.isShow = false;
    }

    show = () => {
        if (this.isShow) {
            return;
        }
        this.isShow = true;
        let rect = new Phaser.Geom.Rectangle(0, 0, this.w, this.h);
        this.bg = this.scene.add.graphics({ fillStyle: { color: 0x091e42, alpha: 0.56 } } as any);
        this.bg.fillRectShape(rect);


        let rectInfo = new Phaser.Geom.Rectangle(this.w * 0.5 - 200, this.h * 0.5 - 150, 400, 300);
        this.info = this.scene.add.graphics({ fillStyle: { color: 0xf5f5f5 }, lineStyle: { width: 2, color: 0xE1E1E1 } } as any);
        this.info.fillRectShape(rectInfo);
        this.info.strokeRectShape(rectInfo);


        let rectBtn1 = new Phaser.Geom.Rectangle(this.w * 0.5 - 80, this.h * 0.5 + 105, 160, 35);
        this.btn1 = this.scene.add.graphics({ fillStyle: { color: 0x0052cc } } as any);
        this.btn1.setInteractive(rectBtn1, Phaser.Geom.Rectangle.Contains);
        this.btn1.fillRectShape(rectBtn1);

        this.texts.push(this.scene.make.text({
            x: this.w * 0.5,
            y: this.h * 0.5 + 123,
            text: this.btn1Text,
            origin: { x: 0.5, y: 0.5 },
            style: {
                font: '18px Arial',
                fill: "#F5F5F5",
                align: 'center',
                wordWrap: { width: 160 }
            }
        }));

        this.texts.push(this.scene.make.text({
            x: this.w * 0.5,
            y: this.h * 0.5 - 120,
            text: this.header,
            origin: { x: 0.5, y: 0 },
            style: {
                font: '24px Arial',
                fill: "#172b4d",
                align: 'center',
                wordWrap: { width: 300 }
            }
        }));

        this.additionalText = this.scene.make.text({
            x: this.w * 0.5,
            y: this.h * 0.5 - 85,
            text: "",
            origin: { x: 0.5, y: 0 },
            style: {
                font: '14px Arial',
                fill: "#888888",
                align: 'center',
                wordWrap: { width: 350 }
            }
        });

        this.texts.push(this.scene.make.text({
            x: this.w * 0.5,
            y: this.h * 0.5 - 50,
            text: this.text,
            origin: { x: 0.5, y: 0 },
            style: {
                font: '16px Arial',
                fill: "#172b4d",
                align: 'center',
                wordWrap: { width: 350 }
            }
        }));


        this.btn1.on('pointerdown',  () => {
            this.close();
            this.btn1Call();
        });

        this.hat = this.scene.add.sprite(this.w * 0.5 + 195, this.h * 0.5 - 130, 'hat');
    };

    close = () => {
        this.isShow = false;
        this.bg.clear();
        this.info.clear();
        this.hat.destroy();
        this.btn1 && this.btn1.destroy();
        this.additionalText && this.additionalText.destroy();

        this.btn2 && this.btn2.destroy();

        this.texts.forEach((text) => {
            text.destroy();
        });
        //this.texts = [];
    };
}

export {Popup};