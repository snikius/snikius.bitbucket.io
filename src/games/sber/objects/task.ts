import {report} from "../scenes/monitoring";

class Task {
    public obj: Phaser.GameObjects.Graphics;
    private active: boolean;
    public positionX: number;
    public positionY: number;
    private group: Phaser.Physics.Arcade.StaticGroup;
    private scene: Phaser.Scene;

    private offsetX: number = -84;
    private offsetY: number = 84;

    private posW: number = 210;
    private posH: number = 75;

    constructor(scene: Phaser.Scene, group: Phaser.Physics.Arcade.StaticGroup, positionX: number, positionY: number, asset: string, frame: string) {
        this.group = group;
        this.scene = scene;
        this.positionX = positionX;
        this.positionY = positionY;
        this.obj = group.create(this.offsetX + this.posW * (positionX + 1), this.offsetY + this.posH * (positionY + 1), asset, frame);
        this.active = true;
    }

    isActive = (): boolean => {
        return this.active;
    };

    moveToXY = (x: number, y: number) => {
        this.active = false;
        this.scene.tweens.add({
            targets: this.obj,
            x,
            y,
            duration: 400,
            ease: 'Power2',
            onComplete: () => {
                this.active = true;
                this.group.refresh();
            },
        });
    };

    moveToPosition = (positionX: number, positionY: number) => {
        report("task_moved");
        this.positionX = positionX;
        this.positionY = positionY;
        this.moveToXY(this.offsetX + this.posW * (this.positionX + 1), this.offsetY + this.posH * (this.positionY + 1));
    };

    clear = () => {
        report("task_completed");
        (this.obj as any).disableBody(true, true);
    };
}

export {Task};