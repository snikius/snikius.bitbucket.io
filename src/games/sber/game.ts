/**
 * @author       Digitsensitive <digit.sensitivee@gmail.com>
 * @copyright    2018 Digitsensitive
 * @description  Coin Runner
 * @license      Digitsensitive
 */

/// <reference path="../../phaser.d.ts"/>

import "phaser";
import { GameScene } from "./scenes/game-scene";
import { MainMenuScene } from "./scenes/mainMenuScene";

const config: GameConfig = {
  title: "SBER GAME",
  url: "https://github.com/digitsensitive/phaser3-typescript",
  version: "0.0.1",
  width: 1100,
  height: 670,
  type: Phaser.AUTO,
  parent: "game",
  scene: [MainMenuScene, GameScene],
  input: {
    keyboard: true
  },
  physics: {
      default: "arcade",
      arcade: {
          debug: false
      }
  },
  backgroundColor: "#19bb4f",
  pixelArt: false
};

export class Game extends Phaser.Game {
  constructor(config: GameConfig) {
    super(config);
  }
}

window.onload = () => {
  var game = new Game(config);
};
